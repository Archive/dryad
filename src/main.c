/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>

#include <gnome.h>
#include "dryad.h"
#include <liboaf/liboaf.h>
#include <libgnomevfs/gnome-vfs.h>
#include <gal/widgets/e-cursors.h>
#include <gal/widgets/e-unicode.h>

/* FIXME: This really needs to be redone, this is just a temporary hack */

#define CORBA_string__alloc() (CORBA_char **)ORBit_alloc(sizeof(gpointer), CORBA_string__free, GUINT_TO_POINTER(1))

#define SERVER_CHECK_TIMEOUT 2000

char *filename = NULL;
char *pid = NULL;
char *corefile = NULL;
char *ver = NULL;

const struct poptOption dryad_popt_options [] = {
    { 
        "appname",        0, POPT_ARG_STRING, &filename, 0, 
        N_("File name of program"), N_("FILE") 
    },
    { 
        "pid",            0, POPT_ARG_STRING, &pid,      0, 
        N_("pid to attach to"),     N_("PID")
    },
    {
        "core",           0, POPT_ARG_STRING, &corefile,  0,
        N_("Corefile to examine"),  N_("FILE")
    },
    {
        "package-ver",    0, POPT_ARG_STRING, &ver,      0,
        N_("package version (for compatibility, unused)"), N_("VERSION")
    },
    { 
        NULL 
    }
};

    

static void 
check_servers (DryadApp *app)
{
    gboolean alive;
    gboolean quit = FALSE;
    
    if (app->dbg) {
        alive = gnome_unknown_ping (app->dbg->objref);
        
        if (!alive) {
            g_warning (_("Debugger no longer valid!  Exiting."));
            app->dbg = NULL;
            quit = TRUE;
        }
    }

    if (quit)
        dryad_exit (app);
}

static guint
init_app (gpointer unused)
{
    GDF_Debugger_StateFlag state;
	DryadApp *app;	
	app = g_new0 (DryadApp, 1);
    
    app->parameters = g_strdup ("");
	app->binary_name = NULL;
    app->dbg = NULL;

    dryad_ui_init (app);

    return FALSE;
#if 0
    if (filename) {
        dryad_load_debugger (app, filename);
    }
    
    if (pid) {
        state = gdf_debugger_client_get_state (app->dbg);
        if ((state & GDF_Debugger_BINARY_LOADED) 
            == GDF_Debugger_BINARY_LOADED) {
            char *errmsg;
            int intpid = atoi (pid);
            GdfDebuggerClientResult res;
            res = gdf_debugger_client_attach (app->dbg, intpid);

            errmsg = NULL;
            switch (res) {
            case GDF_DEBUGGER_CLIENT_OK :
                break;
            case GDF_DEBUGGER_CLIENT_DOESNT_EXIST :
                errmsg = _("Process doesn't exist.");
                break;
            case GDF_DEBUGGER_CLIENT_SECURITY_EXCEPTION :
                errmsg = _("Access denied.");
                break;
            case GDF_DEBUGGER_CLIENT_INVALID_STATE :
                errmsg = _("Binary must be loaded and not running.");
                break;
            case GDF_DEBUGGER_CLIENT_UNKNOWN_ERROR :
                errmsg = _("An unexpected error occured.");
                break;
            default :
                g_assert_not_reached ();
            }
            
            if (errmsg) {
                GtkWidget *dlg;
                errmsg = 
                    g_strdup_printf (_("Could not attach to pid %d: %s"), 
                                     intpid, errmsg);
                dlg = gnome_error_dialog (errmsg);
                gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
            }   
        }
    }

    if (corefile) {
        state = gdf_debugger_client_get_state (app->dbg);
        if ((state & GDF_Debugger_BINARY_LOADED) 
            == GDF_Debugger_BINARY_LOADED) {
            dryad_load_corefile (app, corefile);
        }
    }

    gtk_timeout_add (SERVER_CHECK_TIMEOUT, (GtkFunction)check_servers, app);

	return FALSE;
#endif
}

void
dryad_exit (DryadApp *app)
{
	if (app->dbg)
		gtk_object_unref (GTK_OBJECT (app->dbg));

    dryad_ui_destroy (app);

	gtk_main_quit ();
}             

int
main (int argc, char *argv[])
{
	CORBA_Environment ev;
    poptContext popt_context;
    const char **args;

	CORBA_exception_init (&ev);
#if 0
	orb = gnome_CORBA_init ("dryad", VERSION, &argc, argv,
							0, &ev);
#endif
    gnomelib_register_popt_table (oaf_popt_options, _("OAF options"));
    gnome_init_with_popt_table ("dryad", VERSION, argc, argv,
                                dryad_popt_options, 0, &popt_context);

    args = poptGetArgs (popt_context);

    if (filename == NULL && args && args[0] != NULL) {
        filename = g_strdup (args[0]);
    }

    poptFreeContext (popt_context);

    if (corefile && pid) {
        fprintf (stderr, "%s: Cannot specify both --pid and --core\n",
                 argv[0]);
        exit (1);
    }

    oaf_init (argc, argv);

	if (!bonobo_init (oaf_orb_get (), NULL, NULL)) 
		g_error (_("Can't initialize bonobo!"));

	glade_gnome_init ();

	/* Cannot make CORBA calls unless we're in the main loop.  So we delay. */
	gtk_idle_add ((GtkFunction)init_app, NULL);
	
	bonobo_main ();

	return 0;
}
