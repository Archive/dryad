/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <dave@helixcode.com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gdf/gdf-widgets.h>
#include "dryad.h"
#include <liboaf/liboaf.h>

typedef struct 
{
    char *title;
    char *oaf_id;
    char *wm_name;
    char *wm_class;
    int width;
    int height;
    gboolean visible;
} DryadWidgetDesc;

typedef struct
{
    GtkWidget *window;
    GtkWidget *widget;
    DryadWidgetDesc *desc;
} DryadWidget;

typedef struct
{
    DryadApp *app;
    gchar *filename;
} FileSelectedIdleData;

static void set_initial_sensitivities (DryadApp *app);
static void init_widgets (DryadApp *app);
static void set_debugger (DryadApp *app, 
			  GtkWidget *w);
static void load_cb (GtkWidget *widget, DryadApp *app, char *cname);
static void unload_cb (GtkWidget *widget, DryadApp *app, char *cname);
static void exit_cb (GtkWidget *widget, DryadApp *app, char *cname);
static void about_cb (GtkWidget *widget, gpointer data, char *cname);
static void hide_show_selected_cb (GtkWidget *widget, gpointer data, 
				   char *cname);
static gint main_frame_delete_event_cb (GtkWidget *widget, GdkEvent *event,
                                        DryadApp *app);
static gint window_delete_event_cb (GtkWidget *widget, GdkEvent *event,
                                    gpointer data);
static void file_selected_cb (GtkWidget *widget, GtkWidget *dlg);


/* At some point this table could be loaded from an xml file */
static DryadWidgetDesc widgets [] = {
    { 
	NULL,  /* load it into the main window */
	"OAFIID:control:gdf-default-source-viewer:bde33044-fad3-46b7-8b4a-32b4e923bb79",
	"dryad-app", "dryad-app",
	550, 350, TRUE
    },
    { 
	N_("Breakpoint Manager"), 
	"OAFIID:control:gdf-breakpoint-manager:43548f6f-cb5a-451c-bd2c-1837a85b5e11",
	"dryad-bpm", "dryad-bpm",
	350, 150, TRUE
    },
    { 
	N_("Register Viewer"),
	"OAFIID:control:gdf-register-viewer:1ae95c26-5849-48c8-881d-36656795fa8c",
	"dryad-reg", "dryad-reg",
	145, 300, TRUE
    },
    {
	N_("Program Output"),
	"OAFIID:control:gdf-output-terminal:f4011fcc-ee98-46f1-aa85-99b4e34ae848",
	"dryad-output", "dryad-output",
	500, 225, TRUE
    },
    {
	N_("Stack Browser"),
	"OAFIID:control:gdf-stack-browser:ed6030f5-6bb9-4694-814f-9d0e8a4af28c",
	"dryad-stack", "dryad-stack",
	325, 175, TRUE
    },
#if 0
    { 
	"locals_frame", 
	bonobo_control_create_func, "OAFIID:control:gdf-locals-viewer:51323db4-0c64-4f8c-bdb9-1550506a9b66",
	bonobo_control_debugger_change_func, NULL,
	widget_destruction_func, NULL 
    },
    { 
	"variables_frame", 
	bonobo_control_create_func, "OAFIID:control:gdf-variable-viewer:e471e251-3cf7-4c6a-a6ce-bb6c1328f41a",
	bonobo_control_debugger_change_func, NULL,
	widget_destruction_func, NULL 
    },
#endif
    { NULL, NULL, NULL, NULL, -1, -1, FALSE }
    
};

BonoboUIVerb verbs[] = {
    /* File Menu */
    BONOBO_UI_UNSAFE_VERB ("LoadBinary", load_cb),
    BONOBO_UI_UNSAFE_VERB ("UnloadBinary", unload_cb),
    BONOBO_UI_UNSAFE_VERB ("FileExit", exit_cb),

    /* Help Menu */
    BONOBO_UI_UNSAFE_VERB ("HelpAbout", about_cb),

    BONOBO_UI_VERB_END
};

void 
dryad_ui_init (DryadApp *dryad_app)
{
    Bonobo_UIContainer corba_ui_container;
    CORBA_Environment ev;
    
    dryad_app->win = BONOBO_WINDOW (bonobo_window_new ("dryad", "Dryad v" VERSION));

    dryad_app->container = bonobo_ui_container_new ();
    bonobo_ui_container_set_win (dryad_app->container, dryad_app->win);
    
    corba_ui_container = 
	bonobo_object_corba_objref (BONOBO_OBJECT (dryad_app->container));

    dryad_app->ui_component = bonobo_ui_component_new ("dryad");

    bonobo_ui_component_set_container (dryad_app->ui_component,
				       corba_ui_container);

    CORBA_exception_init (&ev);
    bonobo_ui_util_set_ui (dryad_app->ui_component, 
                           DRYAD_DATADIR, "dryad-ui.xml",
                           "dryad");

    set_initial_sensitivities (dryad_app);

    bonobo_ui_component_add_verb_list_with_data (dryad_app->ui_component, 
                                                 verbs, dryad_app);

    init_widgets (dryad_app);

    gdf_ui_component_init (corba_ui_container);

#if 0

    app->ui = glade_xml_new (DRYAD_GLADEDIR "/dryad.glade", NULL);
    app->widgets = NULL;

    init_main_frame (app);
    init_windows (app);
    init_widgets (app);
#endif
}

void
dryad_ui_destroy (DryadApp *app)
{
    GSList *cur = app->widgets;
    DryadWidget *w;
    
    for (; cur; cur = g_slist_next (cur)) {
	w = (DryadWidget*)cur->data;
	gtk_widget_destroy (w->window);
	g_free (w);
    }

    g_slist_free (app->widgets);

    gdf_ui_component_cleanup ();
}

void
set_initial_sensitivities (DryadApp *app)
{
    bonobo_ui_component_set_prop(app->ui_component, "/commands/LoadBinary", 
				 "sensitive", "1", NULL);
    bonobo_ui_component_set_prop (app->ui_component, "/commands/UnloadBinary",
				  "sensitive", "0", NULL);
}

void
set_debugger (DryadApp *app, 
	      GtkWidget *w)
{   
    gchar *ior;
    CORBA_Environment ev;
    BonoboControlFrame *cf;
    Bonobo_PropertyBag pb;
    
    CORBA_exception_init (&ev);
    if (app->dbg) {
        g_assert (app->dbg->objref != CORBA_OBJECT_NIL);
	ior = CORBA_ORB_object_to_string (oaf_orb_get (), 
					  app->dbg->objref,
					  &ev);
    } else {
	ior = CORBA_string_dup ("");
    }
    
    /* FIXME: Check for exceptions */
    
    cf = bonobo_widget_get_control_frame (BONOBO_WIDGET (w));
    pb = bonobo_control_frame_get_control_property_bag (cf, &ev);
    bonobo_property_bag_client_set_value_string (pb, "debugger-ior", ior, &ev);
    CORBA_free (ior);
    
    CORBA_exception_free (&ev);
}

void
dryad_ui_set_debuggers (DryadApp *app)
{
	GSList *cur;
	DryadWidget *w;
	
	gdf_ui_component_set_debugger (app->dbg);

	for (cur = app->widgets; cur; cur = g_slist_next (cur)) {
	    w = (DryadWidget*)cur->data;
	    set_debugger (app, w->widget);
	}
}

static void
setup_menu_item (DryadApp *app, gchar *label, 
                 GtkWidget *window, GtkWidget *menu)
{
#if 0
    GtkWidget *menu_item;
    
    menu_item = gtk_check_menu_item_new_with_label (label);
    gtk_check_menu_item_set_show_toggle (GTK_CHECK_MENU_ITEM (menu_item),
                                         TRUE);
    gtk_menu_append (GTK_MENU (menu), menu_item);
    gtk_signal_connect (GTK_OBJECT (menu_item), "toggled", 
                        GTK_SIGNAL_FUNC (hide_show_selected_cb), 
                        NULL);
    gtk_widget_show (menu_item);
    
    gtk_object_set_data (GTK_OBJECT (menu_item), "window", window);
    gtk_object_set_data (GTK_OBJECT (window), "menuitem", menu_item);
    
    gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (menu_item),
                                    GTK_WIDGET_VISIBLE (window));  
#endif
}

void
init_widgets (DryadApp *app)
{
	DryadWidgetDesc *desc;
	desc = widgets;

	while (desc->wm_class) {
		DryadWidget *dw;
		GtkWidget *window;
		GtkWidget *widget;
		
		widget = bonobo_widget_new_control (desc->oaf_id, NULL);

		if (desc->title) {
		    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
		    gtk_container_add (GTK_CONTAINER (window), widget);
		    gtk_window_set_title (GTK_WINDOW (window), 
					  desc->title);
		} else {
		    window = app->win;
		    bonobo_window_set_contents (BONOBO_WINDOW (app->win), 
					     widget);
		}
		
		gtk_widget_show (widget);

		gtk_window_set_wmclass (GTK_WINDOW (window),
					desc->wm_name, desc->wm_class);
		gtk_window_set_default_size (GTK_WINDOW (window),
					     desc->width, desc->height);
		
		if (desc->visible) {
		    gtk_widget_show (window);
		}

		dw = g_new0 (DryadWidget, 1);
		dw->window = window;
		dw->widget = widget;
		dw->desc = desc;
		app->widgets = g_slist_prepend (app->widgets, dw);

		desc++;
	}
}

GtkWidget *
bonobo_control_create_func (DryadApp *app, void *data)
{
	GtkWidget *ret;
	
	ret = bonobo_widget_new_control ((char*)data, NULL);
	return ret;
}

typedef GtkWidget *(*NoArgWidgetCreationFunc)(void);
typedef void *(*WidgetDbgFunc)(GtkWidget *w, GdfDebuggerClient *dbg);

void
load_cb (GtkWidget *widget, DryadApp *app, char *cname)
{
	GtkWidget *dlg;
	
	dlg = gtk_file_selection_new (_("Select Program"));
	gtk_object_set_data (GTK_OBJECT (dlg), "dryad_app", (gpointer) app);
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->ok_button),
						"clicked", GTK_SIGNAL_FUNC (file_selected_cb),
						(gpointer)dlg);
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->cancel_button),
						"clicked", GTK_SIGNAL_FUNC (file_selected_cb),
						(gpointer)dlg);
	gtk_widget_show (dlg);
}

void
unload_cb (GtkWidget *widget, DryadApp *app, char *cname)
{
	gdf_debugger_client_unload_binary (app->dbg);

	g_free (app->binary_name);
	app->binary_name = NULL;

    dryad_unload_debugger (app);
}


void
exit_cb (GtkWidget *widget, DryadApp *app, char *cname)
{
	dryad_exit (app);
}

static gint
load_file (FileSelectedIdleData *data)
{
    if (data->app->binary_name != NULL) {
        gdf_debugger_client_unload_binary (data->app->dbg);
        g_free (data->app->binary_name);
        data->app->binary_name = NULL;
    }

    /* FIXME: You don't really need to destroy and create debuggers */
    if (data->app->dbg)
        dryad_unload_debugger (data->app);

    dryad_load_debugger (data->app, data->filename);

    g_free (data->filename);
    g_free (data);

    return FALSE;
}

void 
file_selected_cb (GtkWidget *widget, GtkWidget *dlg)
{
    FileSelectedIdleData *data;

	if (widget != GTK_FILE_SELECTION (dlg)->cancel_button) {
        data = g_new0(FileSelectedIdleData, 1);
		data->filename = 
            g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (dlg)));
		data->app = (DryadApp*)gtk_object_get_data (GTK_OBJECT (dlg), 
                                                    "dryad_app");
        gtk_idle_add ((GtkFunction)load_file, data);
	}
	
	gtk_widget_destroy (dlg);
}

void 
about_cb (GtkWidget *widget, gpointer data, char *cname)
{
	GtkWidget *about;
	static gchar *authors[] = { 
        N_("Dave Camp <dave@helixcode.com>"),
		NULL
	};

	about = 
	    gnome_about_new (_("Dryad"), VERSION, 
			     "Copyright 1999-2000 Dave Camp",
			     (const gchar **)authors,
			     "A graphical debugging tool for the GNOME environment.",
			     NULL);
	gtk_widget_show (about);
}							 

void
hide_show_selected_cb (GtkWidget *widget, gpointer data, char *cname)
{
    GtkWidget *window;
    
    window = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (widget), "window"));
    
    if (GTK_CHECK_MENU_ITEM (widget)->active)
        gtk_widget_show (window);
    else
        gtk_widget_hide (window);
}

gint
main_frame_delete_event_cb (GtkWidget *widget, GdkEvent *event, DryadApp *app)
{
    dryad_exit (app);

    return FALSE;
} 

gint
window_delete_event_cb (GtkWidget *widget, GdkEvent *event, gpointer data)
{
    GtkWidget *menu_item;
    menu_item = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (widget), 
                                                 "menuitem"));
    gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (menu_item), FALSE);
    gtk_widget_hide (widget);

    return TRUE;
}


