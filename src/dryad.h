/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __DRYAD_H__
#define __DRYAD_H__

#include <gdf/gdf.h>
#include <bonobo.h>
#include <glade/glade.h>

#include <bonobo.h>

typedef struct _DryadApp 
{
    BonoboWindow *win;
    BonoboUIComponent *ui_component;
    BonoboUIContainer *container;

    GdfDebuggerClient *dbg;
    char *binary_name;
    char *tty_name;
    char *parameters;
    GSList *widgets;
} DryadApp;

#include "commander-callbacks.h"
#include "ui.h"

void dryad_exit (DryadApp *app);

GtkWidget *create_output_terminal (gchar **output_tty_name);
void destroy_output_terminal (GtkWidget *widget);

#endif
