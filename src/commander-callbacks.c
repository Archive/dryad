/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * This file is part of the GNOME Debugging Framework.
 * 
 * Copyright (C) 1999-2000 Dave Camp <campd@oit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <gnome.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime.h>

#include "dryad.h"

static void connect_debugger_signals (DryadApp *app);
static void program_loaded_cb (GdfDebuggerClient *dbg, DryadApp *app);
static void program_unloaded_cb (GdfDebuggerClient *dbg, DryadApp *app);

gboolean
dryad_load_debugger (DryadApp *app, const char *filename)
{
    GdfDebuggerClientResult res;

    app->dbg = gdf_debugger_client_new_for_file (filename);
    
    if (!app->dbg) {
#warning "Me too"
#if 0
        gnome_app_error (GNOME_APP (app_widget),
                         _("Unable to start a GDF backend server for this "
                           "mime type.\n"
                           "Make sure you installed gnome-debug in the "
                           "correct \n"
                           "location and that you are trying to debug "
                           "a file with \n"
                           "a mime-type that gnome-debug supports."));
#endif
        return FALSE;
    }

    connect_debugger_signals (app);
    dryad_ui_set_debuggers (app);

    res = gdf_debugger_client_load_binary (app->dbg, filename);
    if (res == GDF_DEBUGGER_CLIENT_OK) {
        gdf_debugger_client_set_breakpoint_function (app->dbg, 
                                                     NULL, "main",
                                                     NULL, NULL);
        app->binary_name = g_strdup (filename);
    } else {
        GtkWidget *app_widget;
#warning "me too"
#if 0        
	app_widget = glade_xml_get_widget (app->ui, "dryad_app");
        gnome_app_error (GNOME_APP (app_widget), 
                         _("Could not load file."));
#endif
    }

    return TRUE;
}

void
dryad_unload_debugger (DryadApp *app)
{
    gtk_object_unref (GTK_OBJECT (app->dbg));
    app->dbg = NULL;
    dryad_ui_set_debuggers (app);
}

void
dryad_load_corefile (DryadApp *app, const char *corefile)
{  
    char *errmsg;
    GdfDebuggerClientResult res;
    res = gdf_debugger_client_load_corefile (app->dbg, corefile);
    
    errmsg = NULL;
    switch (res) {
    case GDF_DEBUGGER_CLIENT_OK :
        break;
    case GDF_DEBUGGER_CLIENT_DOESNT_EXIST :
        errmsg = _("Could not find corefile.");
        break;
    case GDF_DEBUGGER_CLIENT_SECURITY_EXCEPTION :
        errmsg = _("Access denied.");
        break;
    case GDF_DEBUGGER_CLIENT_INVALID_STATE :
        errmsg = _("Binary must be loaded and not running.");
        break;
    case GDF_DEBUGGER_CLIENT_UNKNOWN_ERROR :
        errmsg = _("An unexpected error occured.");
        break;
    case GDF_DEBUGGER_CLIENT_INVALID_COREFILE :
        errmsg = _("Not a valid core file.");
        break;
    default :
        g_assert_not_reached ();
    }
    
    if (errmsg) {
        GtkWidget *dlg;
        errmsg = 
            g_strdup_printf (_("Could not load corefile %s: %s"), 
                             corefile, errmsg);
        dlg = gnome_error_dialog (errmsg);
        gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
    }   
}

void
connect_debugger_signals (DryadApp *app)
{
    gtk_signal_connect (GTK_OBJECT (app->dbg), "program_loaded",
			GTK_SIGNAL_FUNC (program_loaded_cb), (gpointer)app);
    gtk_signal_connect (GTK_OBJECT (app->dbg), "program_unloaded",
			GTK_SIGNAL_FUNC (program_unloaded_cb), (gpointer)app);
    
#if 0
    gtk_signal_connect (GTK_OBJECT (app->dbg), "corefile_loaded",
                        GTK_SIGNAL_FUNC (corefile_loaded_cb), (gpointer)app);
    gtk_signal_connect (GTK_OBJECT (app->dbg), "corefile_unloaded",
                        GTK_SIGNAL_FUNC (corefile_unloaded_cb), (gpointer)app);
    gtk_signal_connect (GTK_OBJECT (app->dbg), "execution_started",
			GTK_SIGNAL_FUNC (execution_started_cb),
			(gpointer)app);
    gtk_signal_connect (GTK_OBJECT (app->dbg), "execution_exited",
			GTK_SIGNAL_FUNC (execution_exited_cb),
			(gpointer)app);
    gtk_signal_connect (GTK_OBJECT (app->dbg), "signal_received",
                        GTK_SIGNAL_FUNC (signal_received_cb),
                        (gpointer)app);
    gtk_signal_connect (GTK_OBJECT (app->dbg), "signal_termination",
                        GTK_SIGNAL_FUNC (signal_termination_cb),
                        (gpointer)app);
    gtk_signal_connect (GTK_OBJECT (app->dbg), "attached",
                        GTK_SIGNAL_FUNC (attached_cb),
                        (gpointer)app);
    gtk_signal_connect (GTK_OBJECT (app->dbg), "detached",
                        GTK_SIGNAL_FUNC (detached_cb),
                        (gpointer)app);
#endif
}

static void 
program_loaded_cb (GdfDebuggerClient *dbg, DryadApp *app)
{   
    bonobo_ui_component_set_prop(app->ui_component, "/commands/LoadBinary", 
				 "sensitive", "0", NULL);
    bonobo_ui_component_set_prop (app->ui_component, "/commands/UnloadBinary",
				  "sensitive", "1", NULL);
}

static void 
program_unloaded_cb (GdfDebuggerClient *dbg, DryadApp *app)
{
    bonobo_ui_component_set_prop(app->ui_component, "/commands/LoadBinary", 
				 "sensitive", "1", NULL);
    bonobo_ui_component_set_prop (app->ui_component, "/commands/UnloadBinary",
				  "sensitive", "0", NULL);
}
